import { Injectable, Inject } from '@angular/core';
//import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { InterFaceViewModel } from '../models/Interface'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HostService {

    HOST_STORAGE_NAME:string="__host_list__"
    constructor( ) { }

    getHosts() {
        return JSON.parse(localStorage.getItem(this.HOST_STORAGE_NAME));
        //return this.http.get(this.baseUri + "api/host/getHosts").map(x => x.json());
    }

    saveHost(hostData: InterFaceViewModel) {
        let allHosts: InterFaceViewModel[] = this.getHosts();
        if(!allHosts) allHosts=[];
        let hostIndex = allHosts.findIndex(x => x.id == hostData.id);
        if (hostIndex > 0) {
            allHosts[hostIndex].interfaceLoopBack = hostData.interfaceLoopBack;
            allHosts[hostIndex].interfaceName = hostData.interfaceName;
            allHosts[hostIndex].interfaces = hostData.interfaces;
        }
        else {
            hostData.id=new Date().getUTCMilliseconds();
            allHosts.push(hostData);
        }
        
        localStorage.setItem(this.HOST_STORAGE_NAME,JSON.stringify(allHosts));
        //return this.http.post(this.baseUri + 'api/host/save', hostData).map(x => x.json());
    }

    saveHostInterFaces(hostInterFaces:InterFaceViewModel[],hostId:number)
    {
        let allHosts: InterFaceViewModel[] = this.getHosts();
        let hostIndex = allHosts.findIndex(x => x.id == hostId);
        if (hostIndex >= 0) {
            allHosts[hostIndex].interfaces = hostInterFaces;
        }

        localStorage.setItem(this.HOST_STORAGE_NAME,JSON.stringify(allHosts));
    }

    GetInterFaces(id: number) {
        //return this.http.get(this.baseUri + 'api/host/GetInterFaces?id=' + id).map(x => x.json());
    }

    DeleteInterFaces(id: number) {
       /// return this.http.post(this.baseUri + 'api/host/delete?id=' + id, null).map(x => x.json());
    }

    checkIfExists(objType: string, value: string) {
      //  return this.http.get(this.baseUri + 'api/host/checkIfExists?objType=' + objType + '&value=' + value);
    }
}