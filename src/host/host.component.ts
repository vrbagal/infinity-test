import { Component, OnInit } from '@angular/core';
import { HostService } from './host.service'
import { InterFaceViewModel } from '../models/Interface';
@Component({
    selector: 'host-data',
    templateUrl: './host2.html',
    providers:[HostService]
})

export class HostComponent implements OnInit {

    hostList: InterFaceViewModel[];


    constructor(private service: HostService) { 
        
        this.hostList=[];
    }

    ngOnInit() {
        
        this.hostList=[];
        this.getHostData();
    }

    updateInterface(hostData: InterFaceViewModel) {
        this.service.saveHost(hostData) 
            this.getHostData();
        

    }

    delete(index: number) {
     let id=this.hostList[index].id;
        // this.service.DeleteInterFaces(index).subscribe(x => {
        //     this.getHostData();
        // });
    }
    getHostData() {
        this.hostList =this.service.getHosts()
        // .subscribe((x: InterFaceViewModel[]) => {
        //     = x;
        // })
    }


}

