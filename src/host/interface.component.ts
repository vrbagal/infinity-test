import { Component, OnInit, Output, Input, TemplateRef } from '@angular/core';
import { InterFaceViewModel } from '../models/Interface'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HostService } from './host.service';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'interface-component',
    templateUrl: './interface.html'
})

export class InterfaceComponent implements OnInit {
    typeName: string
   modalRef: BsModalRef;
    currentHost: InterFaceViewModel;
    hostInterface: InterFaceViewModel;
    customRegex:'^([0-9]{1,3}\.){3}[0-9]{1,3}(([0-9]|[1-2][0-9]|3[0-2]))?$';

    constructor( private modalService: BsModalService,private service: HostService) { }///

    ngOnInit() {
        this.typeName = this.isHost ? "Host" : "Interface";
        this.currentHost = new InterFaceViewModel();
        this.hostInterface = new InterFaceViewModel();
    }

    @Input()
    interfaces: InterFaceViewModel[];

    @Input()
    isHost: boolean

    @Output("onInterfaceModified")
    interfaceModified: EventEmitter<any> = new EventEmitter<any>();

    @Output("onInterfaceDelete")
    interfaceDeleted: EventEmitter<any> = new EventEmitter<any>();

    editInterFace(interfaceModel: TemplateRef<any>, host: InterFaceViewModel) {
        this.hostInterface = host;
        this.modalRef = this.modalService.show(interfaceModel);
    }

    callEnterfaceModified() {

        this.interfaceModified.emit(this.currentHost);
        this.currentHost = new InterFaceViewModel();
    }

    saveHostList() {

        if (this.interfaceModified) {

            this.interfaceModified.emit(this.hostInterface);
            if (this.modalRef)
                 this.modalRef.hide();
            this.hostInterface = new InterFaceViewModel();
        }
    }

    hostInterFaceModified(hostInterface: InterFaceViewModel) {
        let index = this.hostInterface.interfaces.findIndex(x => x.id == hostInterface.id);

        if (index > -1) {
           
            this.hostInterface.interfaces[index].interfaceName = hostInterface.interfaceName;
            this.hostInterface.interfaces[index].interfaceLoopBack = hostInterface.interfaceLoopBack;
        }
        else {
            hostInterface.hostId = this.currentHost.id;
            var timestamp = new Date().getUTCMilliseconds();
            hostInterface.id=timestamp;
            this.hostInterface.interfaces.push(hostInterface);
        }

    }

    hostInterfaceDelete(i: number) {
        this.hostInterface.interfaces.splice(i, 1);
    }

    delete(i: number) {
        let confirmed = confirm("Do you want to delete " + this.typeName);
        if (confirmed)
            this.interfaceDeleted.emit(i);
    }

    edit(host: InterFaceViewModel) {
        Object.assign(this.currentHost, host);
    }

}