import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap'
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { HostComponent } from "../host/host.component"
import { HostService } from "../host/host.service"
import { InterfaceComponent } from "../host/interface.component" 
import { ErrorsComponent } from '../host/error.component'
@NgModule({
  declarations: [
    AppComponent,
    HostComponent,
    InterfaceComponent,
    ErrorsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ModalModule.forRoot() 
  ],
  providers: [HostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
