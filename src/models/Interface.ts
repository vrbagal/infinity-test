
export class InterFaceViewModel
{
    constructor(){
        this.interfaces=[];
    }
    hostId:number;
    id:number;
    isHost:boolean;
    interfaceName:string;
    interfaceLoopBack:string;
    interfaces:InterFaceViewModel[];
}